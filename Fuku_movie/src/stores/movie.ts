import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Movie from "@/types/Movie";
import movieService from "@/service/movieService";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useMovieStore = defineStore("Movie", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const movies = ref<Movie[]>([]);
  const editedMovie = ref<Movie>({
    image: "",
    title: "",
    length: 0,
    description: ""
  });

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedMovie.value = {image: "", title: "", length: 0, description: "", };
    }
  });

  async function getMovies() {
    loadingStore.isLoading = true;
    try {
      const res = await movieService.getMovies();
      movies.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Movie ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveMovie() {
    loadingStore.isLoading = true;
    try {
      if (editedMovie.value.id) {
        const res = await movieService.updateMovie(
          editedMovie.value.id,
          editedMovie.value
        );
      } else {
        const res = await movieService.saveMovie(editedMovie.value);
      }
      dialog.value = false;
      await getMovies();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Movie ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function daleteMovie(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await movieService.deleteMovie(id);
      await getMovies();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Movie ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editMovie(movie: Movie) {
    editedMovie.value = JSON.parse(JSON.stringify(movie));
    dialog.value = true;
  }
  return {
    movies,
    getMovies,
    dialog,
    editedMovie,
    saveMovie,
    editMovie,
    daleteMovie,
  };
});
