import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Hall from '@/types/Hall';
import hallService from '@/service/hallService';

export const useHallStore = defineStore('Hall', () => {
  const halls = ref<Hall[]>([]);
  const getHalls = async () => {
    try {
      const res = await hallService.getHalls();
      halls.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  return { halls, getHalls }
})
