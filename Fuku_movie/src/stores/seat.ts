import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Seat from '@/types/Seat';
import seatService from '@/service/seatService';

export const useSeatStore = defineStore('Seat', () => {
  const seats = ref<Seat[]>([]);
  const getSeats = async () => {
    try {
      const res = await seatService.getSeats();
      seats.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  return { seats, getSeats }
})
