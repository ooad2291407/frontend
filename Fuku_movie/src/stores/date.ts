import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Date from '@/types/Date';
import dateService from '@/service/dateService';

export const useDateStore = defineStore('Date', () => {
  const dates = ref<Date[]>([]);
  const getDates = async () => {
    try {
      const res = await dateService.getDates();
      dates.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  return { dates, getDates }
})
