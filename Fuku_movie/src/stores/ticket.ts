import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Ticket from '@/types/Ticket';
import ticketService from '@/service/ticketService';
import { id } from 'vuetify/locale';


export const useTicketStore = defineStore('Ticket', () => {
  const dialog = ref(false);
  const dialog1 = ref(false);
  const dialog2 = ref(false);
  const dialog3 = ref(false);
  const tickets = ref<Ticket[]>([]);
  const getTickets = async () => {
    try {
      const res = await ticketService.getTickets();
      tickets.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  const getTicketById = async (id:number) => {
    try {
      const res = await ticketService.findTicketById(id);
      const ticket = res.data as Ticket
      return ticket.id
    } catch (error) {
      console.log(error)
      return null;
    }
  }

  return { tickets, getTickets, dialog, dialog1, dialog2, dialog3, getTicketById }
})
