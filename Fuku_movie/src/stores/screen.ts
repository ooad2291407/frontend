import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import screenService from '@/service/screenService';
import type Screen from '@/types/Screen';

export const useScreenStore = defineStore('Screen', () => {
  const screens = ref<Screen[]>([]);
  const getScreens = async () => {
    try {
      const res = await screenService.getScreens();
      screens.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  return { screens, getScreens }
})
