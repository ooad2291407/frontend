export default interface Ticket {
  id: number;
  used_status: string;
  qrcode: string;
  createdAt: Date;
  expired_date: Date;
}