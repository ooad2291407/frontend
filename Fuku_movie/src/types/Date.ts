export default interface Date {
  id: number;
  date: string;
}