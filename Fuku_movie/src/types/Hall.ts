export default interface Hall {
  id: number;
  name: string;
  createdAt: Date;
}