export default interface Movie {
  id?: number;
  image: string;
  title: string;
  length: number;
  description: string;
  createdAt?: Date;
  updatedAt?: Date;
}