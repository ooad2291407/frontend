import http from './axios'

function getHalls() {
  return http.get('/halls')
}

export default { getHalls } 