import http from './axios'

function getScreens() {
  return http.get('/screens')
}

export default { getScreens } 