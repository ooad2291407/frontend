// import type Seat from '@/types/Seat'
import http from './axios'

function getSeats() {
  return http.get('/seats');
}

// function saveSeat(seat: Seat) {
//   console.log(seat)
//   return http.post('/seats', seat)
// }

// function updateSeat(id: number, seat: Seat) {
//   return http.patch(`/seats/${id}`, seat)
// }

// function deleteSeat(id: number) {
//   return http.delete(`/seats/${id}`)
// }

export default { getSeats }
