import type Movie from '@/types/Movie' // Assuming you have a Movie type defined
import http from './axios'

function getMovies() {
  return http.get('/movies')
}

function getMovieNameById(movie: Movie): string | undefined {
  // Assuming your Movie type has an 'id' and 'name' property
  return movie.id ? movie.title : undefined
}

function saveMovie(movie: Movie) {
  console.log(movie)
  return http.post('/movies', movie)
}

function updateMovie(id: number, movie: Movie) {
  return http.patch(`/movies/${id}`, movie)
}

function deleteMovie(id: number) {
  return http.delete(`/movies/${id}`)
}

export default { getMovies, saveMovie, updateMovie, deleteMovie, getMovieNameById }
