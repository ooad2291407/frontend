import http from './axios'

function getDates() {
  return http.get('/dates')
}

export default { getDates } 