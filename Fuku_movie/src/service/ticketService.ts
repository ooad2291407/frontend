import http from './axios'

function getTickets() {
  return http.get('/tickets')
}

function findTicketById(id: number) {
  return http.get(`/tickets/${id}`);
}


export default { getTickets,findTicketById } 