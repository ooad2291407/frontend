import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/manager",
      name: "manager",
      components: {
        default: () => import("@/views/checkTicket/CheckTicketView.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
        header: () => import("@/components/headers/ManagerHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/checkticket",
      name: "checkticket",
      components: {
        default: () => import("@/views/checkTicket/CheckTicketView.vue"),
        header: () => import("@/components/headers/ManagerHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/movie",
      name: "movie",
      components: {
        default: () => import("@/views/movie/MovieView.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/MovieDetailView/1",
      name: "MovieDetailView",
      components: {
        default: () => import("@/views/movieDetail/MovieDetailView.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/seat",
      name: "seat",
      components: {
        default: () => import("@/views/seat/SeatView.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
    {
      path: "/test",
      name: "test",
      components: {
        default: () => import("@/views/TestView.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },
  ]
})

export default router
